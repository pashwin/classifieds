package ashwin.client;

import com.extjs.gxt.ui.client.data.BaseModelData;

public class CategoryTreeitem extends BaseModelData {

	String name;
	int id;

	public CategoryTreeitem(String name, int id) {
		this.name = name;
		this.id = id;
		set(name);
		set(id);
	}

	public void set(String name) {
		set("name", name);
	}

	public void set(int id) {
		set("id", id);
	}

}
