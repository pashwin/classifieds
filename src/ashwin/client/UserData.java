package ashwin.client;

import com.extjs.gxt.ui.client.data.BaseModelData;

public class UserData extends BaseModelData {



	public UserData() {
	  }

	  public UserData(String name) {
	    set("name", name);	    
	  }

	  public String getName() {
	    return get("name");
	  }

	  public void setName(String name) {
	    set("name", name);
	  }


	  
}
