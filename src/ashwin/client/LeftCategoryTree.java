package ashwin.client;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.TreeListener;

public class LeftCategoryTree {

	
	
	public LayoutContainer drawTree(){
		Tree myTree = new Tree();
		
		ArrayList<CategoryTreeitem> dataForTree = new TreeDataGenerator().getTreeData();
		
		for(CategoryTreeitem  treeData : dataForTree){
			myTree.add(new CheckBox((String)treeData.get("name")));
		}
		
		myTree.addSelectionHandler(new SelectionHandler<TreeItem>(){
			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
					System.out.println("");
			}
		});
		myTree.addTreeListener(new TreeListener(){
			@Override
			public void onTreeItemSelected(TreeItem item) {

				CheckBox cBox = (CheckBox)item.getWidget();
				
				
				String text = item.getText();
				String text1 = item.getTitle();
				System.out.println(item);
			}
			
			@Override
			public void onTreeItemStateChanged(TreeItem item) {
				CheckBox cBox = (CheckBox)item.getWidget();

				System.out.println(item);
			}
		});
		
		
		LayoutContainer lc = new LayoutContainer();
		lc.add(myTree);
		
		return lc;
		
	}
	
	
	
}
