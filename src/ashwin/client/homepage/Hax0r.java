package ashwin.client.homepage;

import java.util.HashMap;

import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;

import com.google.gwt.core.client.JavaScriptObject;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

public class Hax0r {
	protected HashMap scriptTags = new HashMap();
	protected HashMap callbacks = new HashMap();
	protected int curIndex = 0;

	public native static void setup(Hax0r h, String callback) /*-{
		$wnd[callback] = function(someData) {
		  h.@ashwin.client.homepage.Hax0r::handle(Lcom/google/gwt/core/client/JavaScriptObject;)(someData);
		}
	}-*/;

	public String reserveCallback() {
		while (true) {
			if (!callbacks.containsKey(new Integer(curIndex))) {
				callbacks.put(new Integer(curIndex), null);
				return "__gwt_callback" + curIndex++;
			}
		}
	}

	public void addScript(String uniqueId, String url) {
		Element e = DOM.createElement("script");
		DOM.setAttribute(e, "language", "JavaScript");
		DOM.setAttribute(e, "src", url);
		scriptTags.put(uniqueId, e);
		DOM.appendChild(RootPanel.get().getElement(), e);
	}

	public void onModuleLoad() {

		// call this method 5 times...

		int id = 887325283;

		for (int i = 0; i < 5; i++) {
			String gdata = "http://graph.facebook.com/"+id+"&callback=";
			String callbackName = reserveCallback();
			setup(this, callbackName);
			addScript(callbackName, gdata + callbackName);
			id++;
		}

	}

	public void handle(JavaScriptObject jso) {
		JSONObject json = new JSONObject(jso);
		
		Image myPic = new Image("http://graph.facebook.com/887325283");
		
		Anchor profilePath = new Anchor("Ashwin", "http://www.facebook.com/profile.php?id=887325283", "_blank");
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(myPic);
		vp.add(profilePath);
		
		System.out.println(json.get("id") + "=" + json.get("name") + "="
				+ json.get("first_name") + json.get("last_name")
				+ json.get("link"));

		JSONArray ary = json.get("id").isObject().get("name").isArray();
		for (int i = 0; i < ary.size(); ++i) {
			// RootPanel.get().add(new
			// Label(ary.get(i).isObject().get("title").isObject
			// ().get("$t").toString()));

			System.out.println("PRINITING="
					+ ary.get(i).isObject().get("title").isObject().get("$t")
							.toString());

		}
	}
}
