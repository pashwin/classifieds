package ashwin.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface BazaarServiceAsync {
	void getLeftTreeForBuy(String input, AsyncCallback<String> callback);
}
