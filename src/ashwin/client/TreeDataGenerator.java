package ashwin.client;

import java.util.ArrayList;

public class TreeDataGenerator {

	
	public ArrayList<CategoryTreeitem> getTreeData(){
		CategoryTreeitem item1 = new CategoryTreeitem("Electrical", 1);
		CategoryTreeitem item2 = new CategoryTreeitem("Computers", 2);
		CategoryTreeitem item3 = new CategoryTreeitem("Shata", 3);
		
		ArrayList<CategoryTreeitem> items = new ArrayList<CategoryTreeitem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);
		
		return items;
	}
	
}
