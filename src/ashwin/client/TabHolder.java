package ashwin.client;

import ashwin.client.homepage.HomePage;
import ashwin.server.BazaarServiceImpl;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Label;

public class TabHolder {

	
	
	public LayoutContainer initialise(){
		

		LayoutContainer tabLayout =  new LayoutContainer();
		
		TabPanel tabs = new TabPanel();
		final FormPanel form = new RentForm().init2();
		
		
		TabItem buy = new TabItem("BUY");
		buy.setHeight(500);
		Dialog someDialog = new Dialog() {
			
			
			@Override
			protected void onButtonPressed(Button button) {

				if( button == getButtonBar().getItemByItemId(OK)){
					form.submit();
				}else{
					super.onButtonPressed(button);
				}
				  
			}
		};
		someDialog.add(form);
		someDialog.setResizable(true);
		someDialog.setWidth(400);
		
		
		Button danger = new Button("danger");
		danger.addSelectionListener(new SelectionListener<ButtonEvent>(){
			
			@Override
			public void componentSelected(ButtonEvent ce) {
				
				BazaarServiceAsync bazaarService = GWT
				.create(BazaarService.class);
				
				bazaarService.getLeftTreeForBuy("input", new AsyncCallback<String>(){
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("failure");
						
					}
					
					@Override
					public void onSuccess(String result) {
						System.out.println("success");
						
					}
				});
				
				
				
				
				
			};
		});
		
		someDialog.add(danger);
//		someDialog.setLayout(new FitLayout());
		buy.add(someDialog);
		
		tabs.add(buy);
		
		TabItem sell = new TabItem("SELL");
		sell.add(new HomePage().draw());
		
		tabs.add(sell);
		
		
		
		
		TabItem rent =  new TabItem("RENT");
		rent.add(new SellMyStuff().createForm());
		tabs.add(rent);
		
		tabLayout.setLayout(new FitLayout());
		tabLayout.add(tabs);
		tabLayout.setHeight(500);
		return tabLayout;
	}
}
