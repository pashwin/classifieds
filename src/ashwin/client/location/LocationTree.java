package ashwin.client.location;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;



/**
 * to be picked from a file.
 * currently hardcoding locationsTree
 * @author ashwin
 *
 */
public class LocationTree {

	
	
	public TreePanel<ModelData> createTree(){
		
		TreeStore<ModelData> store = FakeLocationsData.createLocationsTree();
		
		
		
		final TreePanel<ModelData> tree = new TreePanel<ModelData>(store);   
	    tree.setDisplayProperty("name");   
//	    tree.getStyle().setLeafIcon(Resources.ICONS.music());   
	    tree.setCheckable(true);   
	    tree.setAutoLoad(true);   
	    
	    
	    return tree;
		
	}
	
	
}
