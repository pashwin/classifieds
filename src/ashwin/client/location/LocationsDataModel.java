package ashwin.client.location;

import com.extjs.gxt.ui.client.data.BaseModelData;

public class LocationsDataModel  extends BaseModelData {
	public LocationsDataModel() {
	  }
	
	
	public LocationsDataModel(String name) {
	    set("name", name);	    
	  }

	  public String getName() {
	    return get("name");
	  }

	  public void setName(String name) {
	    set("name", name);
	  }
	  
	  
	  
}
