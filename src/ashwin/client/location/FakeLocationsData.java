package ashwin.client.location;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.TreeStore;

public class FakeLocationsData {

	
	
	public static TreeStore<ModelData> createLocationsTree(){
		TreeStore<ModelData> store = new TreeStore<ModelData>();
		
		
		LocationsDataModel bangaloreRoot = new LocationsDataModel("Bangalore");
		store.add(bangaloreRoot, false);
		
		LocationsDataModel inagar = new LocationsDataModel("inagar");
		LocationsDataModel jnagar = new LocationsDataModel("jnagar");
		LocationsDataModel tnagar = new LocationsDataModel("tnagar");
		LocationsDataModel jbnagar = new LocationsDataModel("jbnagar");
		
		store.add(bangaloreRoot, inagar, false);
		store.add(bangaloreRoot, jnagar, false);
		store.add(bangaloreRoot, tnagar, false);
		store.add(bangaloreRoot, jbnagar, false);
		
		
		return store;
	}
	
	
	
}
