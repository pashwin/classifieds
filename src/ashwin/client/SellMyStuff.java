package ashwin.client;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.layout.TableData;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

public class SellMyStuff extends LayoutContainer {

	public Dialog createForm() {

		Dialog sellDialog = new Dialog();
		sellDialog.setButtons(Dialog.OK);

		
		TableData labelData = new TableData();
		labelData.setHeight("20");

		TableData dataData = new TableData();
		dataData.setHeight("50");

		VerticalPanel vp = new VerticalPanel();

		vp.add(new Label("What are you selling"));

		TextArea what = new TextArea();
		vp.add(what);
		vp.add(new Label("Why are you selling"));
		TextArea why = new TextArea();
		vp.add(why);
		vp.add(new Label("How much do you want for it"));
		TextArea howmuch = new TextArea();
		vp.add(howmuch);
		
		vp.add(new Label("How to contact you?"));
		TextArea contact = new TextArea();
		vp.add(contact);
		
		vp.setStyleAttribute("padding-left", "10px");
		sellDialog.add(vp);
		return sellDialog;
	}

}
