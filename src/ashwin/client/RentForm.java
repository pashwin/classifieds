package ashwin.client;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.RadioGroup;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;

import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.TableData;
import com.google.gwt.dom.client.FormElement;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;

public class RentForm {

//	public com.google.gwt.user.client.ui.FormPanel initialise() {
//
//		final com.google.gwt.user.client.ui.FormPanel rentformPanel = new com.google.gwt.user.client.ui.FormPanel();
////		final FormPanel rentformPanel = new FormPanel();
////		rentformPanel.setEncoding(FormPanel.Encoding.MULTIPART);
////		rentformPanel.setMethod(FormPanel.Method.POST);
//		// FormElement.as(rentformPanel.getElement()).setAcceptCharset("UTF-8");
//		rentformPanel.setAction("/iownthissite/rentFormHandler");
//
////		rentformPanel.setLabelWidth(70);
//		FormData formData = new FormData("-20");
//
//		TextArea address = new TextArea();
//		address.setPreventScrollbars(true);
//		address.setFieldLabel("Address");
//		address.setName("addr");
//		// address.setValue("JMJ");
//		rentformPanel.add(address/*, formData*/);
//
////		TextArea desc = new TextArea();
////		desc.setPreventScrollbars(false);
////		desc.setMaxLength(500);
////		desc.setFieldLabel("Description");
////		desc.setName("desc");
////		rentformPanel.add(desc, formData);
////
////		SimpleComboBox<String> typeOfRent = new SimpleComboBox<String>();
////		typeOfRent.setName("renttype");
////		typeOfRent.setTriggerAction(TriggerAction.ALL);
////		typeOfRent.add("Individual");
////		typeOfRent.add("Flat");
////		typeOfRent.add("Paying Guest");
////		typeOfRent.setWidth(100);
////		// typeOfRent.setTitle("Rent Type");
////		typeOfRent.setFieldLabel("Type");
////		rentformPanel.add(typeOfRent, new FormData(150, -1));
////
////		Label bhk = new Label("BHK");
////		Label bathRooms = new Label("BathRooms");
////		bhk.setWidth(70);
////
////		TableData td74 = new TableData();
////		// td24.setWidth("24%");
////		td74.setWidth("74px");
////
////		TableData td29 = new TableData();
////		td29.setWidth("29%");
////
////		SimpleComboBox<String> roomCount = new SimpleComboBox<String>();
////		roomCount.setName("roomcount");
////		roomCount.add("1");
////		roomCount.add("2");
////		roomCount.add("3");
////		roomCount.add(">3");
////		roomCount.setWidth(55);
////		roomCount.setFieldLabel("BHK");
////		// roomCount.setName("BHK");
////
////		roomCount.setTriggerAction(TriggerAction.ALL);
////
////		SimpleComboBox<String> bathroomCount = new SimpleComboBox<String>();
////		bathroomCount.setName("bathroomcount");
////		bathroomCount.add("1");
////		bathroomCount.add("2");
////		bathroomCount.add("3");
////		bathroomCount.add(">3");
////		bathroomCount.setWidth(55);
////		bathroomCount.setFieldLabel("BHK");
////		// bathroomCount.setName("BHK");
////
////		HorizontalPanel bhkkkk = new HorizontalPanel();
////		bhkkkk.add(bhk, td74);
////		bhkkkk.add(roomCount, td29);
////		bhkkkk.add(bathRooms, td29);
////		bhkkkk.add(bathroomCount);
////		rentformPanel.add(bhkkkk, new FormData());
////
////		Label bach = new Label("Bachelors");
////		Radio bachelorsYES = new Radio();
////		bachelorsYES.setId("bachelorsyesid");
////		bachelorsYES.setItemId("bachelorsyesitemid");
////		bachelorsYES.setName("bachelorsYES");
////		bachelorsYES.setBoxLabel("Allowed ");
////
////		Radio bachelorsNO = new Radio();
////		bachelorsNO.setName("bachelorsNO");
////		bachelorsNO.setBoxLabel("Not Allowed ");
////
////		RadioGroup radioGroup = new RadioGroup();
////		radioGroup.setName("radiogroup");
////		radioGroup.setId("radiogroup");
////		radioGroup.add(bachelorsNO);
////		radioGroup.add(bachelorsYES);
////		radioGroup.setFieldLabel("Bachelors");
////
////		/*
////		 * HorizontalPanel hp = new HorizontalPanel(); //
////		 * hp.setStyleAttribute("border-right", "80");
////		 * 
////		 * hp.add(bach, td74 ); hp.add(bachelorsYES); hp.add(bachelorsNO);
////		 */
////		rentformPanel.add(radioGroup, new FormData(150, -1));
////
////		FieldSet parking = new FieldSet();
////		parking.setHeading("Parking");
////		parking.setLayout(new FormLayout());
////
////		SimpleComboBox<String> cars = new SimpleComboBox<String>();
////		cars.setName("cars");
////		cars.setTriggerAction(TriggerAction.ALL);
////		cars.add("1");
////		cars.add("2");
////		cars.add(">2");
////		cars.setWidth(55);
////		cars.setFieldLabel("Cars");
////
////		SimpleComboBox<String> bikes = new SimpleComboBox<String>();
////		bikes.setName("bikes");
////		bikes.setTriggerAction(TriggerAction.ALL);
////		bikes.add("1");
////		bikes.add("2");
////		bikes.add(">2");
////		bikes.setWidth(55);
////		bikes.setFieldLabel("bikes");
////		bikes.setTitle("bikes");
////
////		Label carss = new Label("Cars");
////		Label bikess = new Label("Bikes");
////
////		FormLayout fl = new FormLayout();
////		fl.setLabelWidth(75);
////		parking.setLayout(fl);
////
////		HorizontalPanel parkingTable = new HorizontalPanel();
////		parkingTable.add(carss, td74);
////		parkingTable.add(cars, td29);
////		parkingTable.add(bikess, td29);
////		parkingTable.add(bikes);
////
////		parking.add(parkingTable);
////
////		rentformPanel.add(parking);
////
////		Label food = new Label("Food");
////		Radio veg = new Radio();
////		veg.setName("veg");
////		veg.setBoxLabel("Veg");
////
////		Radio nonveg = new Radio();
////		nonveg.setName("nonveg");
////		nonveg.setBoxLabel("Non veg");
////
////		Radio none = new Radio();
////		none.setName("none");
////		none.setBoxLabel("None");
////
////		RadioGroup foodGroup = new RadioGroup();
////		foodGroup.setName("foodgroup");
////		foodGroup.setId("foodgroup");
////		foodGroup.add(veg);
////		foodGroup.add(nonveg);
////		foodGroup.add(none);
////		foodGroup.setFieldLabel("Food");
////		rentformPanel.add(foodGroup, new FormData(150, -1));
////
////		Radio furnishedYes = new Radio();
////		furnishedYes.setName("furnishedyes");
////		furnishedYes.setBoxLabel("Yes");
////
////		Radio furnishedNO = new Radio();
////		furnishedNO.setName("furnishedno");
////		furnishedNO.setBoxLabel("No");
////
////		RadioGroup furnishedGroup = new RadioGroup();
////		furnishedGroup.setName("furnishedGroup");
////		furnishedGroup.setId("furnishedGroup");
////		furnishedGroup.add(furnishedYes);
////		furnishedGroup.add(furnishedNO);
////		furnishedGroup.setFieldLabel("Furnished");
//
//		HorizontalPanel fileUploadTable = new HorizontalPanel();
//		fileUploadTable.add(new Label("Upload Photo")/*, td29*/);
//		FileUpload fileUpload = new FileUpload();
//		fileUpload.setName("fileupload");
//		fileUploadTable.add(fileUpload);
//
//		rentformPanel.add(fileUploadTable);
//
////		rentformPanel.add(furnishedGroup, new FormData(150, -1));
//
//		Button submit = new Button("Submit");
//		submit.addSelectionListener(new SelectionListener<ButtonEvent>() {
//
//			@Override
//			public void componentSelected(ButtonEvent ce) {
//
//				rentformPanel.submit();
//			}
//
//		});
//
//		rentformPanel.add(submit);
//		// // Add an event handler to the form.
//		// rentformPanel.addSubmitHandler(new FormPanel.SubmitHandler() {
//		//		 
//		// public void onSubmit(SubmitEvent event) {
//		// // This event is fired just before the form is submitted. We can take
//		// // this opportunity to perform validation.
//		// if (tb.getText().length() == 0) {
//		// Window.alert("The text box must not be empty");
//		// event.cancel();
//		// }
//		// }
//		// });
//		// rentformPanel.addSubmitCompleteHandler(new
//		// FormPanel.SubmitCompleteHandler() {
//		//		 
//		// public void onSubmitComplete(SubmitCompleteEvent event) {
//		// // When the form submission is successfully completed, this event is
//		// // fired. Assuming the service returned a response of type text/html,
//		// // we can get the result text here (see the FormPanel documentation
//		// for
//		// // further explanation).
//		// Window.alert(event.getResults());
//		// }
//		// });
//		//		
//
//		return rentformPanel;
//	}
	
	
	public HorizontalPanel getSplitPane(String label, Widget w, String widgetHeight){
		HorizontalPanel hp = new HorizontalPanel();
		TableData td = new TableData();
		td.setWidth("100");
		
		TableData td1 = new TableData();
		td1.setWidth("250");
//		td.setStyle("border: 1px solid red");
//		td1.setStyle("border: 1px solid blue");
		td1.setHeight(widgetHeight);

		hp.setStyleAttribute("padding", "2px");
		hp.add(new Label(label), td);
		hp.add(w, td1);
		
//		hp.setStyleAttribute("border", "1px solid green");

		
		return hp;
		
	}
	
	
	public com.google.gwt.user.client.ui.FormPanel init2(){


		final com.google.gwt.user.client.ui.FormPanel rentformPanel = new com.google.gwt.user.client.ui.FormPanel();
		
		rentformPanel.addSubmitCompleteHandler(new SubmitCompleteHandler(){

			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {


				Window.alert("Rental stuff has been posted");
				
			}
			
		});
		
		
		HorizontalPanel hp = new HorizontalPanel();
		ContentPanel cp = new ContentPanel();
		
		
		rentformPanel.setEncoding(com.google.gwt.user.client.ui.FormPanel.ENCODING_MULTIPART);
		rentformPanel.setMethod(com.google.gwt.user.client.ui.FormPanel.METHOD_POST);
		rentformPanel.setAction("/iownthissite/rentFormHandler");

//		rentformPanel.setLabelWidth(70);
//		FormData formData = new FormData("-20");

		
		com.google.gwt.user.client.ui.Label addressLabel = new com.google.gwt.user.client.ui.Label("Address");
		
		com.google.gwt.user.client.ui.TextArea address = new com.google.gwt.user.client.ui.TextArea();
//		address.setPreventScrollbars(true);
//		address.setFieldLabel("Address");
		address.setTitle("title");
		address.setText("ashwin");
		address.setValue("value");
		
		
//		address.setsss
		address.setName("addr");
		cp.add(getSplitPane("Address", address, "65"));
		
		TextArea desc = new TextArea();
//		desc.setFieldLabel("Description");
		desc.setName("desc");
//		vp.add(desc);//, formData);

		
		cp.add(getSplitPane("Description", desc, "65"));
		
		SimpleComboBox<String> typeOfRent = new SimpleComboBox<String>();
		typeOfRent.setName("renttype");
		typeOfRent.setTriggerAction(TriggerAction.ALL);
		typeOfRent.add("Individual");
		typeOfRent.add("Flat");
		typeOfRent.add("Paying Guest");
		typeOfRent.setWidth(100);
		// typeOfRent.setTitle("Rent Type");
		typeOfRent.setFieldLabel("Type");
//		vp.add(typeOfRent);//, new FormData(150, -1));

		cp.add(getSplitPane("Type", typeOfRent, "25"));
		
//		Label bhk = new Label("BHK");
//		Label bathRooms = new Label("BathRooms");
//		bhk.setWidth(70);
//
//		TableData td74 = new TableData();
//		// td24.setWidth("24%");
//		td74.setWidth("74px");
//
//		TableData td29 = new TableData();
//		td29.setWidth("29%");

		SimpleComboBox<String> roomCount = new SimpleComboBox<String>();
		roomCount.setName("roomcount");
		roomCount.add("1");
		roomCount.add("2");
		roomCount.add("3");
		roomCount.add(">3");
		roomCount.setWidth(55);
		roomCount.setFieldLabel("BHK");
		// roomCount.setName("BHK");

		roomCount.setTriggerAction(TriggerAction.ALL);

		
		SimpleComboBox<String> bathroomCount = new SimpleComboBox<String>();
		bathroomCount.setName("bathroomcount");
		bathroomCount.add("1");
		bathroomCount.add("2");
		bathroomCount.add("3");
		bathroomCount.add(">3");
		bathroomCount.setWidth(55);
		bathroomCount.setTriggerAction(TriggerAction.ALL);
		bathroomCount.setFieldLabel("BHK");
		// bathroomCount.setName("BHK");
		
		
		HorizontalPanel hpBathR = new HorizontalPanel();
		TableData tdd = new TableData();
		tdd.setPadding(2);
		hpBathR.add(roomCount, tdd);
		hpBathR.add(new Label("BathRooms"), tdd);
		hpBathR.add(bathroomCount, tdd);
		
		
		cp.add(getSplitPane("Rooms", hpBathR, "25"));
		
		
//		HorizontalPanel bhkkkk = new HorizontalPanel();
//		bhkkkk.add(bhk, td74);
//		bhkkkk.add(roomCount, td29);
//		bhkkkk.add(bathRooms, td29);
//		bhkkkk.add(bathroomCount);
//		vp.add(bhkkkk);//, new FormData());

		Label bach = new Label("Bachelors");
		RadioButton bachelorsYES = new RadioButton("bachelors", "Yes");
		bachelorsYES.setFormValue("yes");
//		bachelorsYES.setId("bachelorsyesid");
//		bachelorsYES.setItemId("bachelorsyesitemid");
//		bachelorsYES.setName("bachelorsYES");
//		bachelorsYES.setBoxLabel("Allowed ");

		RadioButton bachelorsNo = new RadioButton("bachelors", "No");
		bachelorsNo.setFormValue("no");
//		bachelorsNo.setName("bachelorsNO");
//		bachelorsNO.setBoxLabel("Not Allowed ");
		
		
		
		
//
//		RadioGroup radioGroup = new RadioGroup();
//		radioGroup.setName("radiogroup");
//		radioGroup.setId("radiogroup");
//		radioGroup.add(bachelorsNo);
//		radioGroup.add(bachelorsYES);
//		radioGroup.setFieldLabel("Bachelors");

		HorizontalPanel hp22 = new HorizontalPanel();
		TableData td = new TableData();
		td.setWidth("50");
		
		hp22.add(bachelorsNo, td);
		hp22.add(bachelorsYES, td);
		
		
		cp.add(getSplitPane("Bachelors", hp22, "25"));
		
//
//		FieldSet parking = new FieldSet();
//		parking.setHeading("Parking");
//		parking.setLayout(new FormLayout());
//
		SimpleComboBox<String> cars = new SimpleComboBox<String>();
		cars.setName("cars");
		cars.setTriggerAction(TriggerAction.ALL);
		cars.add("1");
		cars.add("2");
		cars.add(">2");
		cars.setWidth(55);
		cars.setFieldLabel("Cars");

		SimpleComboBox<String> bikes = new SimpleComboBox<String>();
		bikes.setName("bikes");
		bikes.setTriggerAction(TriggerAction.ALL);
		bikes.add("1");
		bikes.add("2");
		bikes.add(">2");
		bikes.setWidth(55);
		bikes.setFieldLabel("bikes");
		bikes.setTitle("bikes");
		
		HorizontalPanel hpaa = new HorizontalPanel();
		
		hpaa.add(new Label("Cars"), td);
		hpaa.add(cars);
		hpaa.add(new Label(" Bikes"), td);
		hpaa.add(bikes);
		
		cp.add(getSplitPane("Parking", hpaa, "22"));

		
		Label food = new Label("Food");
		RadioButton veg = new RadioButton("Food", "Veg");
		veg.setFormValue("veg");
		RadioButton nonVeg = new RadioButton("Food", "NonVeg");
		nonVeg.setFormValue("nonveg");
		
		RadioButton none = new RadioButton("Food", "None");
		none.setFormValue("none");
		HorizontalPanel hpFood = new HorizontalPanel();
		td.setWidth("50");
		
		hpFood.add(veg, td);
		hpFood.add(nonVeg, new TableData("65", ""));
		hpFood.add(none, td);
		
		
		cp.add(getSplitPane("Food", hpFood, "25"));


		
		
		Label furnished = new Label("Furnished");
		RadioButton yes = new RadioButton("Furnished", "Yes");
		yes.setFormValue("furnished");
		RadioButton no = new RadioButton("Furnished", "No");
		no.setFormValue("notfurnished");
		
		HorizontalPanel hpFurnished = new HorizontalPanel();
		td.setWidth("50");
		
		hpFurnished.add(yes, td);
		hpFurnished.add(no, new TableData("65", ""));
		
		cp.add(getSplitPane("Furnished", hpFurnished, "25"));

		
		
		
		HorizontalPanel fileUploadTable = new HorizontalPanel();
		fileUploadTable.add(new Label("Upload Photo")/*, td29*/);
		FileUpload fileUpload = new FileUpload();
		fileUpload.setName("fileupload");
		fileUploadTable.add(fileUpload);

		cp.add(fileUploadTable);

		
		
		
		Button submit = new Button("Submit");
		submit.addSelectionListener(new SelectionListener<ButtonEvent>() {

			@Override
			public void componentSelected(ButtonEvent ce) {

				rentformPanel.submit();
			}

		});

		cp.add(submit);

		
		rentformPanel.setHeight("600");
		
		rentformPanel.setWidget(cp);

		return rentformPanel;
	
	}
	
}
