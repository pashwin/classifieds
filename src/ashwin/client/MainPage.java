package ashwin.client;

import ashwin.client.homepage.HomePage;
import ashwin.client.location.LocationTree;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabBar;

public class MainPage /*extends LayoutContainer*/{

	
	public MainPage(){
//		initialise();
	}
	
	
	
	
	public LayoutContainer initialise(){
		  
		LayoutContainer page = new LayoutContainer();
		final BorderLayout layout = new BorderLayout();  
		page.setLayout(layout);  
		page.setStyleAttribute("padding", "10px");  
	   
	     ContentPanel north = new ContentPanel();  
	     north.setHeaderVisible(false);
	     north.add(new Label("NORTH"));


	     ContentPanel center = new ContentPanel();
	     center.setHeight(1000);
	     center.setHeading("BorderLayout Example");  
	     center.setScrollMode(Scroll.AUTOX);  
	     center.setBorders(false);
	     center.setHeaderVisible(false);

	     center.add(new TabHolder().initialise());
//	    center.add(new HomePage().draw()); 
	     
	     
	     ContentPanel east = new ContentPanel();  
	     east.add(new LeftCategoryTree().drawTree());
	     
	     
	     ContentPanel west = new ContentPanel();  
	     west.add(new LocationTree().createTree());
	     
	     
	     
	     BorderLayoutData northData = new BorderLayoutData(LayoutRegion.NORTH, 40);  
	     northData.setCollapsible(true);  
	     northData.setFloatable(true);  
	     northData.setHideCollapseTool(true);  
	     northData.setSplit(true);  
	     northData.setMargins(new Margins(0, 0, 5, 0));  
	   
	     BorderLayoutData centerData = new BorderLayoutData(LayoutRegion.CENTER);  
	     centerData.setMargins(new Margins(0));  
	   
	     BorderLayoutData eastData = new BorderLayoutData(LayoutRegion.EAST);
	     eastData.setSplit(true);  
	     eastData.setCollapsible(true);  
	     eastData.setMargins(new Margins(0,0,0,5));
	     
	     BorderLayoutData westData = new BorderLayoutData(LayoutRegion.WEST);  
	     westData.setSplit(true);  
	     westData.setCollapsible(true);  
	     westData.setMargins(new Margins(0,0,0,5));
	     
	     page.add(north, northData);  
	     page.add(center, centerData);  
	     page.add(east, eastData);
	     page.add(west, westData);
	     
	     return page;
	     
	}
}
