package ashwin.server;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;



@PersistenceCapable
public class RentModel {

	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;

	
	@Persistent
	private String address;
    
    @Persistent
	private String descripton;
    
    
    @Persistent
	private String type;//Later pick this from constants
    
    @Persistent
	private int roomCount;
	
    @Persistent
	private boolean bachelors;
    
    @Persistent
	private boolean food;
	
    @Persistent
	private int cars;
    
    @Persistent
	private int bikes;
    
    @Persistent
    private MyImage image;
    
    
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}
	public MyImage getImage() {
		return image;
	}
	public void setImage(MyImage image) {
		this.image = image;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getRoomCount() {
		return roomCount;
	}
	public void setRoomCount(int roomCount) {
		this.roomCount = roomCount;
	}
	public boolean isBachelors() {
		return bachelors;
	}
	public void setBachelors(boolean bachelors) {
		this.bachelors = bachelors;
	}
	public boolean isFood() {
		return food;
	}
	public void setFood(boolean food) {
		this.food = food;
	}
	public int getCars() {
		return cars;
	}
	public void setCars(int cars) {
		this.cars = cars;
	}
	public int getBikes() {
		return bikes;
	}
	public void setBikes(int bikes) {
		this.bikes = bikes;
	}
	
	public Key getKey() {
        return key;
    }

	
}
