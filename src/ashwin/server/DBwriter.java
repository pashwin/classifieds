package ashwin.server;


import java.util.Date;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;



public class DBwriter {

	
	
	
	public void writeRentDetails(){
		
		DummyData dd = new DummyData();
		RentModel rm = dd.genRentdata();
		
		
        PersistenceManager pm = PMF.get().getPersistenceManager();
        try {
            pm.makePersistent(rm);
        } finally {
            pm.close();
        }
	}
}
