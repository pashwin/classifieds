package ashwin.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
//import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;

import com.google.appengine.api.datastore.Blob;

public class RentFormServlet extends HttpServlet implements Servlet{

	
	
	private RentModel rm = new RentModel();
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)
			      throws ServletException, IOException {
			    response.setContentType("text/html");
			    
			    
			    Map paramMap = request.getParameterMap();
			    
			    Set keySet = paramMap.keySet();
			    for(Object o : keySet){
			    	System.out.println("KEY="+keySet.toString());
			    	System.out.println("VALUE="+paramMap.get(o));
			    	
			    }
			    try{
			    ServletFileUpload upload = new ServletFileUpload();
			    
			    FileItemIterator iter = upload.getItemIterator(request);
			    while (iter.hasNext()) {
			        FileItemStream item = iter.next();
			        String name = item.getFieldName();
			        InputStream stream = item.openStream();
			        if (item.isFormField()) {
			            System.out.println("Form field " + name + " with value "
			                + Streams.asString(stream) + " detected.");
			        } else {
			            System.out.println("File field " + name + " with file name "
			                + item.getName() + " detected.");
			            // Process the input stream
			            
			        }
			    }

			    }catch(Exception e) {
			    	e.printStackTrace();
			    	}
			    
			    System.out.println(request.getParameter("addr"));
			    FileItem uploadItem = getFileItem(request);
			    if (uploadItem == null) {
			      response.getWriter().write("NO-SCRIPT-DATA");
			      return;
			    }

			    byte[] fileContents = uploadItem.get();
			    //TODO: add code to process file contents here. We will just print
			//it.
			    System.out.println(new String(fileContents));
			    response.getWriter().write(new String(fileContents));
			  }

			  private FileItem getFileItem(HttpServletRequest req) {
			    
			    try{
				  ServletFileUpload upload = new ServletFileUpload();
				    FileItemIterator iter = upload.getItemIterator(req);
				    
				    for( ; iter.hasNext(); ){
				    	FileItemStream imageItm = iter.next();
					    
				    	if(imageItm.getFieldName().equals("fileupload")){
				    		InputStream imgStream = imageItm.openStream();
						    
						    

						    // construct our entity objects
						    Blob imageBlob = new Blob(IOUtils.toByteArray(imgStream));
						    MyImage myImage = new MyImage("MYIMAGE", imageBlob);

				    	}else{
				    		InputStream imgStream = imageItm.openStream();
				    		IOUtils.toByteArray(imgStream);
				    		System.out.println(IOUtils.toString(imgStream));
				    	}
				    }
				    
				    
				    

			    }catch(Exception e){
			    	e.printStackTrace();
			    }
				  return null;
			  } 
			  
			  
			  
			  
			 

	
}
